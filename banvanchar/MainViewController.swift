//
//  MainViewController.swift
//  banvanchar
//
//  Created by 정김기보 on 12/12/2019.
//  Copyright © 2019 정김기보. All rights reserved.
//

import UIKit
import SnapKit

class MainViewController: UITabBarController {
    var commuteViewController: CommuteViewController!
    var homeViewController: HomeViewController!
    var myViewController: MyViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        commuteViewController = CommuteViewController()
        homeViewController = HomeViewController()
        myViewController = MyViewController()
        
        commuteViewController.tabBarItem.image = UIImage(named: "btnIcCart")
        homeViewController.tabBarItem.image = UIImage(named: "btnIcCart")
        myViewController.tabBarItem.selectedImage = UIImage(named: "btnIcCart")
        
        viewControllers = [commuteViewController, homeViewController, myViewController]
    }
    
}
